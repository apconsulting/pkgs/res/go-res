package store

import (
	"encoding/json"
	"errors"

	res "gitlab.com/apconsulting/pkgs/res/go-res"
)

type Nullable[T any] struct {
	Valid bool
	Value T
}

// Returns value v of type T as a Nullable[T] with Valid = true
func AsNullable[T any](v T) Nullable[T] {
	return Nullable[T]{
		Value: v,
		Valid: true,
	}
}

// Handler is a res.Service handler that fetches resources,
// and listens for updates from a Store.
type Handler[T any] struct {
	Store       Store[T]
	Transformer Transformer[T]
	Default     Nullable[T]
}

var _ res.Option = Handler[any]{}

type storeHandler[T any] struct {
	s     *res.Service
	p     res.Pattern
	st    Store[T]
	typ   res.ResourceType
	def   Nullable[T]
	trans Transformer[T]
}

var errInvalidResourceType = res.InternalError(errors.New("invalid store resource type"))

// WithStore returns a new Handler value with Store set to store.
func (sh Handler[T]) WithStore(store Store[T]) Handler[T] {
	sh.Store = store
	return sh
}

// WithTransformer returns a new Handler value with Transformer set to transformer.
func (sh Handler[T]) WithTransformer(transformer Transformer[T]) Handler[T] {
	sh.Transformer = transformer
	return sh
}

// WithDefault returns a new Handler value with Default value set to default.
// The default value should be pre-transformed.
func (sh Handler[T]) WithDefault(def T) Handler[T] {
	sh.Default = Nullable[T]{
		Valid: true,
		Value: def,
	}
	return sh
}

// SetOption is to implement the res.Option interface
func (sh Handler[T]) SetOption(h *res.Handler) {
	if sh.Store == nil {
		panic("no Store is set")
	}
	o := storeHandler[T]{
		st:    sh.Store,
		trans: sh.Transformer,
	}
	// if sh.Default.IsValid() {
	// 	raw, err := json.Marshal(sh.Default)
	// 	if err != nil {
	// 		panic("error marshaling default handler value: " + err.Error())
	// 	}

	// 	o.def = raw
	// }
	h.Option(
		res.GetResource(o.getResource),
		res.OnRegister(o.onRegister),
	)
	o.st.OnChange(o.changeHandler)
}

func (o *storeHandler[T]) onRegister(s *res.Service, p res.Pattern, h res.Handler) {
	switch h.Type {
	case res.TypeModel:
		// if len(o.def) > 0 && o.def[0] != '{' {
		// 	panic("Default value for TypeModel must be a json object.")
		// }
	case res.TypeCollection:
		// if len(o.def) > 0 && o.def[0] != '[' {
		// 	panic("Default value for TypeModel must be a json object.")
		// }
	case res.TypeUnset:
		panic("no Type is set")
	default:
		panic("Type must be set to TypeModel or TypeCollection")
	}
	o.s = s
	o.p = p
	o.typ = h.Type
}

func (o *storeHandler[T]) getResource(r res.GetRequest) {
	id := r.ResourceName()
	if o.trans != nil {
		id = o.trans.RIDToID(res.Ref(id), r.PathParams())
		if id == "" {
			r.NotFound()
			return
		}
	}

	txn := o.st.Read(id)
	defer txn.Close()

	var val any
	v, err := txn.Value()
	if err != nil {
		if errors.Is(err, ErrNotFound) && o.def.Valid {
			val = o.def.Value
		} else {
			r.Error(err)
			return
		}
	} else {
		if o.trans != nil {
			val, err = o.trans.Transform(id, v)
			if err != nil {
				r.Error(err)
				return
			}
		} else {
			val = v
		}
	}
	switch o.typ {
	case res.TypeModel:
		r.Model(val)
	case res.TypeCollection:
		r.Collection(val)
	default:
		r.Error(errInvalidResourceType)
	}
}

func (o *storeHandler[T]) changeHandler(id string, before Nullable[T], after Nullable[T], info interface{}) {
	var err error
	rid := id

	var bVal, aVal any

	if before.Valid {
		bVal = before.Value
	}
	if after.Valid {
		aVal = after.Value
	}

	if o.trans != nil {
		if before.Valid {
			bVal, err = o.trans.Transform(id, before.Value)
			if err != nil {
				bVal = nil
			}
		} else if o.def.Valid {
			bVal = o.def.Value
		}
		if after.Valid {
			aVal, err = o.trans.Transform(id, after.Value)
			if err != nil {
				aVal = nil
			}
		} else if o.def.Valid {
			aVal = o.def.Value
		}
		if after.Valid {
			rid = string(o.trans.IDToRID(id, after.Value, o.p))
		} else if before.Valid {
			rid = string(o.trans.IDToRID(id, before.Value, o.p))
		}
		if rid == "" {
			return
		}
	}

	r, err := o.s.Resource(rid)
	if err != nil {
		o.s.Logger().Errorf("error getting resource %s: %s", rid, err)
		return
	}

	if bVal == nil {
		// Assert that both values are not nil. Shouldn't happen.
		if aVal == nil {
			// TODO: Error?
			return
		}
		r.CreateEvent(aVal)
		return
	}

	if aVal == nil {
		r.DeleteEvent()
		return
	}

	switch o.typ {
	case res.TypeModel:
		if err := modelDiff(r, bVal, aVal, info); err != nil {
			o.s.Logger().Errorf("diff failed for model %s: %s", rid, err)
		}
	case res.TypeCollection:
		if err := collectionDiff(r, bVal, aVal, info); err != nil {
			o.s.Logger().Errorf("diff failed for collection %s: %s", rid, err)
		}
	default:
		o.s.Logger().Errorf("invalid resource type")
	}
}

// modelDiff produces change event by comparing before and after value, as they
// look when marshaled into json.
func modelDiff(r res.Resource, before, after, info interface{}) error {
	var beforeMap, afterMap map[string]Value
	var ok bool

	// Convert before and after value to map[string]Value
	if beforeMap, ok = before.(map[string]Value); !ok {
		beforeDta, err := json.Marshal(before)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(beforeDta, &beforeMap); err != nil {
			return err
		}
	}
	if afterMap, ok = after.(map[string]Value); !ok {
		afterDta, err := json.Marshal(after)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(afterDta, &afterMap); err != nil {
			return err
		}
	}

	// Generate change event
	ch := make(map[string]interface{}, len(afterMap))
	for k := range beforeMap {
		if _, ok := afterMap[k]; !ok {
			ch[k] = DeleteValue
		}
	}
	for k, v := range afterMap {
		ov, ok := beforeMap[k]
		if !ok || !v.Equal(ov) {
			ch[k] = v
		}
	}

	// TODO: Auguri qua bisogna smontare tutto
	r.ChangeEvent(ch, info)
	return nil
}

// collectionDiff produces remove and add events by comparing before and after
// value, as they look when marshaled into json.
func collectionDiff(r res.Resource, before, after, info interface{}) error {
	var a, b []Value
	var ok bool

	// Convert before and after value to []Value
	if a, ok = before.([]Value); !ok {
		beforeDta, err := json.Marshal(before)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(beforeDta, &a); err != nil {
			return err
		}
	}
	if b, ok = after.([]Value); !ok {
		afterDta, err := json.Marshal(after)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(afterDta, &b); err != nil {
			return err
		}
	}

	// Generate remove/add events
	var i, j int
	// Do a LCS matrix calculation
	// https://en.wikipedia.org/wiki/Longest_common_subsequence_problem
	s := 0
	m := len(a)
	n := len(b)

	// Trim of matches at the start and end
	for s < m && s < n && a[s].Equal(b[s]) {
		s++
	}

	if s == m && s == n {
		return nil
	}

	for s < m && s < n && a[m-1].Equal(b[n-1]) {
		m--
		n--
	}

	var aa, bb []Value
	if s > 0 || m < len(a) {
		aa = a[s:m]
		m = m - s
	} else {
		aa = a
	}
	if s > 0 || n < len(b) {
		bb = b[s:n]
		n = n - s
	} else {
		bb = b
	}

	// Create matrix and initialize it
	w := m + 1
	c := make([]int, w*(n+1))

	for i = 0; i < m; i++ {
		for j = 0; j < n; j++ {
			if aa[i].Equal(bb[j]) {
				c[(i+1)+w*(j+1)] = c[i+w*j] + 1
			} else {
				v1 := c[(i+1)+w*j]
				v2 := c[i+w*(j+1)]
				if v2 > v1 {
					c[(i+1)+w*(j+1)] = v2
				} else {
					c[(i+1)+w*(j+1)] = v1
				}
			}
		}
	}

	idx := m + s
	i = m
	j = n
	rems := 0

	var adds [][3]int
	addCount := n - c[w*(n+1)-1]
	if addCount > 0 {
		adds = make([][3]int, 0, addCount)
	}
Loop:
	for {
		m = i - 1
		n = j - 1
		switch {
		case i > 0 && j > 0 && aa[m].Equal(bb[n]):
			idx--
			i--
			j--
		case j > 0 && (i == 0 || c[i+w*n] >= c[m+w*j]):
			adds = append(adds, [3]int{n, idx, rems})
			j--
		case i > 0 && (j == 0 || c[i+w*n] < c[m+w*j]):
			idx--
			r.RemoveEvent(idx, info)
			rems++
			i--
		default:
			break Loop
		}
	}

	// Do the adds
	l := len(adds) - 1
	for i := l; i >= 0; i-- {
		add := adds[i]
		r.AddEvent(bb[add[0]], add[1]-rems+add[2]+l-i, info)
	}

	return nil
}
