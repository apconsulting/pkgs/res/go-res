package store

import (
	"fmt"
	"reflect"

	res "gitlab.com/apconsulting/pkgs/res/go-res"
)

// TransformFuncs implements the Transformer interface by calling the functions
// for transforming store requests.
type transformer[T any] struct {
	ridToID   func(rid res.Ref, pathParams map[string]string) string
	idToRID   func(id string, v T, p res.Pattern) res.Ref
	transform func(id string, v T) (any, error)
}

var _ Transformer[any] = transformer[any]{}

// TransformFuncs returns a Transformer that uses the provided functions. Any
// nil function will pass the value untransformed.
func TransformFuncs[T any](
	ridToID func(rid res.Ref, pathParams map[string]string) string,
	idToRID func(id string, v T, p res.Pattern) res.Ref,
	transform func(id string, v T) (any, error),
) Transformer[T] {
	return transformer[T]{
		ridToID:   ridToID,
		idToRID:   idToRID,
		transform: transform,
	}
}

func (t transformer[T]) RIDToID(rid res.Ref, pathParams map[string]string) string {
	if t.ridToID == nil {
		return string(rid)
	}
	return t.ridToID(rid, pathParams)
}

func (t transformer[T]) IDToRID(id string, v T, p res.Pattern) res.Ref {
	if t.idToRID == nil {
		return res.Ref(id)
	}
	return t.idToRID(id, v, p)
}

func (t transformer[T]) Transform(id string, v T) (any, error) {
	if t.transform == nil {
		return v, nil
	}
	return t.transform(id, v)
}

// IDTransformer returns a transformer where the resource ID contains a single
// tag that is the internal ID.
//
//	// Assuming pattern is "library.book.$bookid"
//	IDTransformer("bookId", nil) // transforms "library.book.42" <=> "42"
func IDTransformer[T any](tagName string, transform func(id string, v T) (any, error)) Transformer[T] {
	return TransformFuncs(
		func(_ res.Ref, pathParams map[string]string) string {
			return pathParams[string(tagName)]
		},
		func(id string, _ T, p res.Pattern) res.Ref {
			return res.Ref(p.ReplaceTag(string(tagName), id))
		},
		transform,
	)
}

// IDToRIDCollectionTransformer is a QueryTransformer that handles the common
// case of transforming a slice of id strings:
//
//	[]string{"1", "2"}
//
// into slice of resource references:
//
//	[]res.Ref{"library.book.1", "library.book.2"}
//
// The function converts a single ID returned by a the store into an external
// resource ID.
type IDToRIDCollectionTransformer[T any] func(id string) res.Ref

// TransformResult transforms a slice of id strings into a slice of resource
// references.
func (t IDToRIDCollectionTransformer[T]) TransformResult(v []string) ([]res.Ref, error) {
	refs := make([]res.Ref, len(v))
	for i, id := range v {
		refs[i] = t(id)
	}
	return refs, nil
}

func (t IDToRIDCollectionTransformer[T]) TransformEvent(ev ResultEvent[T]) ResultEvent[res.Ref] {
	return ResultEvent[res.Ref]{
		Name:    ev.Name,
		Idx:     ev.Idx,
		Value:   t(ev.ID),
		ID:      ev.ID,
		Changed: ev.Changed,
	}
}

// IDToRIDModelTransformer is a QueryTransformer that handles the common case of
// transforming a slice of unique id strings:
//
//	[]string{"1", "2"}
//
// into a map of resource references with id as key:
//
//	map[string]res.Ref{"1": "library.book.1", "2": "library.book.2"}
//
// The function converts a single ID returned by a the store into an external
// resource ID.
//
// The behavior is undefined for slices containing duplicate id string.
type IDToRIDModelTransformer func(id string) string

// TransformResult transforms a slice of id strings into a map of resource
// references with id as key.
func (t IDToRIDModelTransformer) TransformResult(v interface{}) (interface{}, error) {
	ids, ok := v.([]string)
	if !ok {
		return nil, fmt.Errorf("failed to transform results: expected value of type []string, but got %s", reflect.TypeOf(v))
	}
	refs := make(map[string]res.Ref, len(ids))
	for _, id := range ids {
		refs[id] = res.Ref(t(id))
	}
	return refs, nil
}

// TransformEvents transforms events for a []string collection into events for a
// map[string]res.Ref model.
func (t IDToRIDModelTransformer) TransformEvents(evs []ResultEvent[string]) ([]ResultEvent[map[string]any], error) {
	ch := make(map[string]interface{}, len(evs))

	if len(evs) == 0 {
		return []ResultEvent[map[string]any]{}, nil
	}

	for _, ev := range evs {
		switch ev.Name {
		case "add":
			id := ev.Value
			// if !ok {
			// 	return nil, fmt.Errorf("failed to transform add event: expected value of type string, but got %s", reflect.TypeOf(ev.Value))
			// }
			ch[id] = res.Ref(t(id))
		case "remove":
			id := ev.Value
			// if !ok {
			// 	return nil, fmt.Errorf("failed to transform remove event: expected value of type string, but got %s", reflect.TypeOf(ev.Value))
			// }
			ch[id] = res.DeleteAction
		}
	}
	return []ResultEvent[map[string]any]{{
		Name:    "change",
		Changed: ch,
	}}, nil
}
