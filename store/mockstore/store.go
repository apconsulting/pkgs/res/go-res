package mockstore

import (
	"errors"
	"fmt"
	"sync"

	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

// Store is an in-memory CRUD mock store implementation.
//
// It implements the store.Store interface.
//
// A Store must not be copied after first call to Read or Write.
type Store[T any] struct {
	// OnChangeCallbacks contains callbacks added with OnChange.
	OnChangeCallbacks []func(id string, before store.Nullable[T], after store.Nullable[T], info interface{})

	// RWMutex protects the Resources map.
	sync.RWMutex

	// Resources is a map of stored resources.
	Resources map[string]T

	// NewID is a mock function returning an new ID when Create is called with
	// an empty ID. Default is that Create returns an error.
	NewID func() string

	// OnExists overrides the Exists call. Default behavior is to return true if
	// Resources contains the id.
	OnExists func(st *Store[T], id string) bool

	// OnValue overrides the Value call. Default behavior is to return the value
	// in Resources, or store.ErrNotFound if not found.
	OnValue func(st *Store[T], id string) (T, error)

	// OnCreate overrides the Create call. Default behavior is to set Resources
	// with the value if the id does not exist, otherwise return a
	// store.ErrDuplicate error.
	OnCreate func(st *Store[T], id string, v T) error

	// OnUpdate overrides the Update call. It should return the previous value,
	// or an error. Default behavior is to replace the Resources value if it
	// exists, or return store.ErrNotFound if not found.
	OnUpdate func(st *Store[T], id string, v T) (T, error)

	// OnDelete overrides the OnDelete call. It should return the deleted value,
	// or an error. Default behavior is to delete the Resources value if it
	// exists, or return store.ErrNotFound if not found.
	OnDelete func(st *Store[T], id string) (T, error)
}

// Assert *Store implements the store.Store interface.
var _ store.Store[any] = &Store[any]{}

var errMissingID = errors.New("missing ID")

type readTxn[T any] struct {
	st     *Store[T]
	id     string
	closed bool
}

type writeTxn[T any] struct {
	readTxn[T]
}

// NewStore creates a new empty Store.
func NewStore[T any]() *Store[T] {
	return &Store[T]{}
}

// Add inserts a value into the Resources map.
func (st *Store[T]) Add(id string, v T) *Store[T] {
	if st.Resources == nil {
		st.Resources = make(map[string]T, 1)
	}
	st.Resources[id] = v
	return st
}

// Read makes a read-lock for the resource that lasts until Close is called.
func (st *Store[T]) Read(id string) store.ReadTxn[T] {
	st.RLock()
	return &readTxn[T]{st: st, id: id}
}

// Write makes a write-lock for the resource that lasts until Close is called.
func (st *Store[T]) Write(id string) store.WriteTxn[T] {
	st.Lock()
	return &writeTxn[T]{readTxn[T]{st: st, id: id}}
}

// Close closes the read transaction.
func (rt *readTxn[T]) Close() error {
	if rt.closed {
		return errors.New("already closed")
	}
	rt.closed = true
	rt.st.RUnlock()
	return nil
}

// Close closes the write transaction.
func (wt *writeTxn[T]) Close() error {
	if wt.closed {
		return errors.New("already closed")
	}
	wt.closed = true
	wt.st.Unlock()
	return nil
}

// Exists returns true if the value exists in the store, or false in case or
// read error or value does not exist.
func (rt readTxn[T]) Exists() bool {
	if rt.id == "" {
		return false
	}

	if rt.st.OnExists != nil {
		return rt.st.OnExists(rt.st, rt.id)
	}

	_, ok := rt.st.Resources[rt.id]
	return ok
}

// Value gets an existing value in the store.
//
// If the value does not exist, store.ErrNotFound is returned.
func (rt readTxn[T]) Value() (T, error) {
	val := *new(T)
	if rt.id == "" {
		return val, store.ErrNotFound
	}

	if rt.st.OnValue != nil {
		return rt.st.OnValue(rt.st, rt.id)
	}

	v, ok := rt.st.Resources[rt.id]
	if !ok {
		return val, store.ErrNotFound
	}

	return v, nil
}

// ID returns the ID of the resource.
func (rt readTxn[T]) ID() string {
	return rt.id
}

// Create adds a new value to the store.
//
// If a value already exists for the resource ID, id, an error is returned.
func (wt writeTxn[T]) Create(v *T) error {
	if wt.id == "" {
		if wt.st.NewID == nil {
			return errMissingID
		}
		wt.id = wt.st.NewID()
		if wt.id == "" {
			panic("callback NewID returned empty string")
		}
	}

	var err error
	if wt.st.OnCreate != nil {
		err = wt.st.OnCreate(wt.st, wt.id, *v)
	} else {
		_, ok := wt.st.Resources[wt.id]
		if ok {
			err = store.ErrDuplicate
		} else {
			if wt.st.Resources == nil {
				wt.st.Resources = make(map[string]T, 1)
			}
			wt.st.Resources[wt.id] = *v
		}
	}

	if err != nil {
		return err
	}

	wt.st.callOnChange(wt.id, store.Nullable[T]{Valid: false}, store.AsNullable(*v))

	return nil
}

// Update overwrites an existing value in the store with a new value, v.
//
// If the value does not exist, res.ErrNotFound is returned.
func (wt writeTxn[T]) Update(v T) error {
	if wt.id == "" {
		return store.ErrNotFound
	}

	var err error
	var before T
	var ok bool

	if wt.st.OnUpdate != nil {
		before, err = wt.st.OnUpdate(wt.st, wt.id, v)
	} else {
		before, ok = wt.st.Resources[wt.id]
		if !ok {
			err = store.ErrNotFound
		} else {
			wt.st.Resources[wt.id] = v
		}
	}

	if err != nil {
		return err
	}

	wt.st.callOnChange(wt.id, store.AsNullable(before), store.AsNullable(v))
	return nil
}

func (wt writeTxn[T]) UpdatePartial(v map[string]any) error {
	return fmt.Errorf("not implemented")
}

// Delete removes an existing value from the store.
//
// If the value does not exist, res.ErrNotFound is returned.
func (wt writeTxn[T]) Delete() error {
	if wt.id == "" {
		return store.ErrNotFound
	}

	var err error
	var before T
	var ok bool

	if wt.st.OnDelete != nil {
		before, err = wt.st.OnDelete(wt.st, wt.id)
	} else {
		before, ok = wt.st.Resources[wt.id]
		if !ok {
			err = store.ErrNotFound
		} else {
			delete(wt.st.Resources, wt.id)
		}
	}

	if err != nil {
		return err
	}

	wt.st.callOnChange(wt.id, store.AsNullable(before), store.Nullable[T]{})
	return nil
}

// OnChange adds a listener callback that is called whenever a value is created,
// updated, or deleted from the store.
//
// If a value is created, before will be set to nil.
//
// If a value is deleted, after will be set to nil.
func (st *Store[T]) OnChange(cb func(id string, before, after store.Nullable[T], info interface{})) {
	st.OnChangeCallbacks = append(st.OnChangeCallbacks, cb)
}

// callOnChange loops through OnChange listeners and calls them.
func (st *Store[T]) callOnChange(id string, before, after store.Nullable[T]) {
	for _, cb := range st.OnChangeCallbacks {
		cb(id, before, after, nil)
	}
}
