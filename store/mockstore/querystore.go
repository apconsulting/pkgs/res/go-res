package mockstore

import (
	"net/url"

	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

// QueryStore mocks a query store.
//
// It implements the store.QueryStore interface.
type QueryStore[T any] struct {
	// OnQueryChangeCallbacks contains callbacks added with OnQueryChange.
	OnQueryChangeCallbacks []func(store.QueryChange[T])

	// OnQuery handles calls to Query.
	OnQuery func(q url.Values) ([]string, error)
}

// Assert *QueryStore implements the store.QueryStore interface.
var _ store.QueryStore[any] = &QueryStore[any]{}

// Assert *QueryChange implements the store.QueryChange interface.
var _ store.QueryChange[any] = &QueryChange[any]{}

// QueryChange mocks a change in a resource that affects the query.
//
// It implements the store.QueryChange interface.
type QueryChange[T any] struct {
	IDValue        string
	BeforeValue    store.Nullable[T]
	AfterValue     store.Nullable[T]
	OnAffectsQuery func(q url.Values) bool
	OnEvents       func(q url.Values) ([]store.ResultEvent[T], bool, error)
}

// Assert QueryChange implements the store.QueryChange interface.
var _ store.QueryChange[any] = QueryChange[any]{}

// NewQueryStore creates a new QueryStore and initializes it.
func NewQueryStore[T any](cb func(q url.Values) ([]string, error)) *QueryStore[T] {
	return &QueryStore[T]{
		OnQuery: cb,
	}
}

// Query returns a collection of references to store ID's matching
// the query. If error is non-nil the reference slice is nil.
func (qs *QueryStore[T]) Query(q url.Values) ([]string, error) {
	return qs.OnQuery(q)
}

// OnQueryChange adds a listener callback that is triggered using the
// TriggerQueryChange.
func (qs *QueryStore[T]) OnQueryChange(cb func(store.QueryChange[T])) {
	qs.OnQueryChangeCallbacks = append(qs.OnQueryChangeCallbacks, cb)
}

// TriggerQueryChange call all OnQueryChange listeners with the QueryChange.
func (qs *QueryStore[T]) TriggerQueryChange(qc QueryChange[T]) {
	for _, cb := range qs.OnQueryChangeCallbacks {
		cb(qc)
	}
}

// ID returns the IDValue string.
func (qc QueryChange[T]) ID() string {
	return qc.IDValue
}

// Before returns the BeforeValue.
func (qc QueryChange[T]) Before() store.Nullable[T] {
	return qc.BeforeValue
}

// After returns the AfterValue.
func (qc QueryChange[T]) After() store.Nullable[T] {
	return qc.AfterValue
}

// Events calls the OnEvents callback, or returns nil and false if OnEvents is
// nil.
func (qc QueryChange[T]) Events(q url.Values) ([]store.ResultEvent[T], bool, error) {
	if qc.OnEvents == nil {
		return nil, false, nil
	}
	return qc.OnEvents(q)
}
