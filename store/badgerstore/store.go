package badgerstore

import (
	"encoding"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/dgraph-io/badger"
	"github.com/jirenius/keylock"
	res "gitlab.com/apconsulting/pkgs/res/go-res"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

// Store is a database CRUD store implementation for BadgerDB.
//
// It implements the store.Store interface.
//
// A Store must not be copied after first call to Read or Write.
type Store[T any] struct {
	DB *badger.DB
	// typ          interface{}
	// t            reflect.Type
	kl           keylock.KeyLock
	prefix       string
	beforeChange []func(id string, before, after store.Nullable[T], info interface{}) error
	onChange     []func(id string, before, after store.Nullable[T], info interface{})
}

var _ store.Store[any] = &Store[any]{}

type readTxn[T any] struct {
	st     *Store[T]
	v      store.Nullable[T]
	id     string
	rname  []byte
	closed bool
}

type writeTxn[T any] struct {
	readTxn[T]
}

// var interfaceMapType = reflect.TypeOf(map[string]interface{}(nil))

// NewStore creates a new Store and initializes it.
//
// The type of typ will be used as value. If the type supports both the
// encoding.BinaryMarshaler and the encoding.BinaryUnmarshaler, those method
// will be used for marshaling the values. Otherwise, encoding/json will be
// used for marshaling.
func NewStore[T any](db *badger.DB) *Store[T] {
	return &Store[T]{
		DB: db,
	}
}

// SetPrefix sets the prefix that will be prepended to all resource ID's, using
// a dot (.) as separator between the prefix and the rest of the ID.
func (st *Store[T]) SetPrefix(prefix string) *Store[T] {
	if prefix == "" {
		st.prefix = ""
	} else {
		st.prefix = prefix + "."
	}
	return st
}

// SetType sets the type, typ, that will be used to unmarshal stored values
// into. If the type supports both the encoding.BinaryMarshaler and the
// encoding.BinaryUnmarshaler, those method will be used for marshaling the
// values. Otherwise, encoding/json will be used for marshaling.
// func (st *Store[T]) SetType(typ interface{}) *Store[T] {
// 	t := reflect.TypeOf(typ)
// 	typ = reflect.New(t).Elem().Interface()
// 	_, bmi := typ.(encoding.BinaryMarshaler)
// 	_, bui := typ.(encoding.BinaryUnmarshaler)
// 	st.typ = typ
// 	st.t = reflect.TypeOf(typ)
// 	st.useMarshal = bmi && bui
// 	return st
// }

// Type returns a zero-value of the type used by the store for unmarshaling
// values.
// func (st *Store[T]) Type() interface{} {
// 	if st.typ == nil {
// 		return map[string]interface{}(nil)
// 	}
// 	return st.typ
// }

// Get reads the value from the store without locking.
func (st *Store[T]) Get(id string) (T, error) {
	var v T
	err := st.DB.View(func(txn *badger.Txn) error {
		var err error
		v, err = st.getValue(txn, []byte(st.prefix+id))
		return err
	})
	if err != nil {
		if err == badger.ErrKeyNotFound {
			return v, res.ErrNotFound
		}
		return v, err
	}
	return v, nil
}

// Read makes a read-lock for the resource that lasts until Close is called.
func (st *Store[T]) Read(id string) store.ReadTxn[T] {
	st.kl.RLock(id)
	return &readTxn[T]{st: st, id: id, rname: []byte(st.prefix + id)}
}

// Write makes a write-lock for the resource that lasts until Close is called.
func (st *Store[T]) Write(id string) store.WriteTxn[T] {
	st.kl.Lock(id)
	return &writeTxn[T]{readTxn[T]{st: st, id: id, rname: []byte(st.prefix + id)}}
}

// Close closes the read transaction.
func (rt *readTxn[T]) Close() error {
	if rt.closed {
		return errors.New("already closed")
	}
	rt.closed = true
	rt.st.kl.RUnlock(rt.id)
	return nil
}

// Close closes the write transaction.
func (wt *writeTxn[T]) Close() error {
	if wt.closed {
		return errors.New("already closed")
	}
	wt.closed = true
	wt.st.kl.Unlock(wt.id)
	return nil
}

// Exists returns true if the value exists in the store, or false in case or
// read error or value does not exist.
func (rt readTxn[T]) Exists() bool {
	return rt.st.DB.View(func(txn *badger.Txn) error {
		_, err := rt.st.getValue(txn, rt.rname)
		return err
	}) == nil
}

// Value gets an existing value in the database.
//
// If the value does not exist, res.ErrNotFound is returned.
func (rt readTxn[T]) Value() (T, error) {
	if rt.v.Valid {
		return rt.v.Value, nil
	}
	var v T
	err := rt.st.DB.View(func(txn *badger.Txn) error {
		var err error
		v, err = rt.st.getValue(txn, rt.rname)
		return err
	})
	if err != nil {
		if err == badger.ErrKeyNotFound {
			return v, res.ErrNotFound
		}
		return v, err
	}
	rt.v = store.AsNullable(v)
	return v, nil
}

// ID returns the ID of the resource.
func (rt readTxn[T]) ID() string {
	return rt.id
}

// Create adds a new value to the database.
//
// If a value already exists for the resource ID, id, an error is returned.
func (wt writeTxn[T]) Create(v *T) error {
	// vv := reflect.ValueOf(v)
	// t := wt.st.t
	// if t == nil {
	// 	t = interfaceMapType
	// }
	// if vv.Type() != t {
	// 	return fmt.Errorf("create value is of type %s, expected type %s", vv.Type().String(), t.String())
	// }

	err := wt.st.DB.Update(func(txn *badger.Txn) error {
		// Validate that the resource doesn't exist
		_, err := txn.Get(wt.rname)
		if err == nil {
			return fmt.Errorf("cannot create because value for %s already exists", wt.id)
		}
		if err != badger.ErrKeyNotFound {
			return err
		}

		// Call beforeChange listeners and cancel if error occurred.
		err = wt.st.callBeforeChange(wt.id, store.Nullable[T]{}, store.AsNullable(*v))
		if err != nil {
			return err
		}

		// Marshal the value and store it in the database
		return wt.st.setValue(txn, wt.rname, *v)
	})
	if err != nil {
		return err
	}

	wt.st.callOnChange(wt.id, store.Nullable[T]{}, store.AsNullable(*v))
	return nil
}

// Update overwrites an existing value in the database with a new value, v.
//
// If the value does not exist, res.ErrNotFound is returned.
func (wt writeTxn[T]) Update(v T) error {
	// vv := reflect.ValueOf(v)
	// t := wt.st.t
	// if t == nil {
	// 	t = interfaceMapType
	// }
	// if vv.Type() != t {
	// 	return fmt.Errorf("update value is of type %s, expected type %s", vv.Type().String(), t.String())
	// }
	var before T
	err := wt.st.DB.Update(func(txn *badger.Txn) error {
		var err error
		// Get before value
		if wt.v.Valid {
			before = wt.v.Value
		} else {
			if before, err = wt.st.getValue(txn, wt.rname); err != nil {
				return err
			}
			wt.v = store.AsNullable(before)
		}

		// Call beforeChange listeners and cancel if error occurred.
		err = wt.st.callBeforeChange(wt.id, store.AsNullable(before), store.AsNullable(v))
		if err != nil {
			return err
		}

		// Marshal new value and update
		return wt.st.setValue(txn, wt.rname, v)
	})
	if err != nil {
		if err == badger.ErrKeyNotFound {
			return res.ErrNotFound
		}
		return err
	}

	wt.st.callOnChange(wt.id, store.AsNullable(before), store.AsNullable(v))
	return nil
}

func (wt writeTxn[T]) UpdatePartial(v map[string]any) error {
	return fmt.Errorf("not implemented")
}

// Delete removes an existing value from the database.
//
// If the value does not exist, res.ErrNotFound is returned.
func (wt writeTxn[T]) Delete() error {
	var before T
	err := wt.st.DB.Update(func(txn *badger.Txn) error {
		var err error

		// Get before value
		if wt.v.Valid {
			before = wt.v.Value
		} else {
			if before, err = wt.st.getValue(txn, wt.rname); err != nil {
				return err
			}
			wt.v = store.AsNullable(before)
		}

		// Call beforeChange listeners and cancel if error occurred.
		err = wt.st.callBeforeChange(wt.id, store.AsNullable(before), store.Nullable[T]{})
		if err != nil {
			return err
		}

		// Delete value
		err = txn.Delete(wt.rname)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		if err == badger.ErrKeyNotFound {
			return res.ErrNotFound
		}
		return err
	}

	wt.st.callOnChange(wt.id, store.AsNullable(before), store.Nullable[T]{})
	return nil
}

// OnChange adds a listener callback that is called whenever a value is created,
// updated, or deleted from the database.
//
// If a value is created, before will be set to nil.
//
// If a value is deleted, after will be set to nil.
func (st *Store[T]) OnChange(cb func(id string, before, after store.Nullable[T], info interface{})) {
	st.onChange = append(st.onChange, cb)
}

// Init adds initial resources for the store. If the store has been previously
// initialized, no resources will be added. It uses a key, "$<prefix>.init"
// (where <prefix> is the set prefix), to mark the store as initialized.
func (st *Store[T]) Init(cb func(add func(id string, v T)) error) error {
	created := make(map[string]T)
	return st.DB.Update(func(txn *badger.Txn) error {
		var err error
		initKey := []byte(`$` + st.prefix + `init`)
		// Check init flag key
		_, err = txn.Get(initKey)
		if err != badger.ErrKeyNotFound {
			return err
		}

		// Load entries
		entries := make(map[string]T)
		// t := st.t
		// if t == nil {
		// 	t = interfaceMapType
		// }
		var adderr error
		add := func(id string, v T) {
			// Quick exit if we have encountered an error previously
			if adderr != nil {
				return
			}
			// Validation
			// vv := reflect.ValueOf(v)
			// if vv.Type() != t {
			// 	adderr = fmt.Errorf("init value is of type %s, expected type %s", vv.Type().String(), t.String())
			// 	return
			// }
			if id == "" {
				adderr = errors.New("empty ID string")
				return
			}
			if _, ok := entries[id]; ok {
				adderr = fmt.Errorf("duplicate id: %s", id)
				return
			}
			entries[id] = v
		}
		// Call init callback with the add method
		if err := cb(add); err != nil {
			return err
		}
		if adderr != nil {
			return adderr
		}

		// Write resources
		for id, v := range entries {
			rname := []byte(st.prefix + id)
			// Skip values that already exists.
			_, err := txn.Get(rname)
			if err == nil {
				continue
			}
			if err != badger.ErrKeyNotFound {
				return err
			}
			if err := st.setValue(txn, rname, v); err != nil {
				return err
			}
			created[id] = v
		}

		// Call OnChange callback
		for id, v := range created {
			st.callOnChange(id, store.Nullable[T]{}, store.AsNullable(v))
		}

		// Set init flag key
		return txn.Set(initKey, nil)
	})
}

// getValue gets a value from the database and unmarshals it.
func (st *Store[T]) getValue(txn *badger.Txn, key []byte) (T, error) {
	var v T

	item, err := txn.Get(key)
	if err != nil {
		if err == badger.ErrKeyNotFound {
			return v, res.ErrNotFound
		}
		return v, err
	}

	if err = item.Value(func(dta []byte) error {
		switch sv := any(&v).(type) {
		case encoding.BinaryUnmarshaler:
			err := sv.UnmarshalBinary(dta)
			if err != nil {
				return err
			}
		default:
			err := json.Unmarshal(dta, sv)
			if err != nil {
				return err
			}
		}

		return nil

		// t := st.t
		// if t == nil {
		// 	t = interfaceMapType
		// }
		// tv := reflect.New(t)
		// if st.useMarshal {
		// 	v = tv.Elem().Interface()
		// 	err := v.(encoding.BinaryUnmarshaler).UnmarshalBinary(dta)
		// 	if err != nil {
		// 		return err
		// 	}
		// } else {
		// 	err := json.Unmarshal(dta, tv.Interface())
		// 	if err != nil {
		// 		return err
		// 	}
		// 	v = tv.Elem().Interface()
		// }
		// return nil
	}); err != nil {
		return v, err
	}
	return v, nil
}

// BeforeChange adds a listener callback that is called before a value is
// created, updated, or deleted from the database.
//
// If the callback returns an error, the change will be canceled.
func (st *Store[T]) BeforeChange(cb func(id string, before, after store.Nullable[T], info interface{}) error) {
	st.beforeChange = append(st.beforeChange, cb)
}

// setValue marshals a value and updates the database.
func (st *Store[T]) setValue(txn *badger.Txn, key []byte, v T) error {
	var err error
	var dta []byte

	switch sv := any(&v).(type) {
	case encoding.BinaryMarshaler:
		dta, err = sv.MarshalBinary()
	default:
		dta, err = json.Marshal(sv)
	}
	if err != nil {
		return err
	}

	return txn.Set(key, dta)

	// return nil
	// if st.useMarshal {
	// 	dta, err = v.(encoding.BinaryMarshaler).MarshalBinary()
	// } else {
	// 	dta, err = json.Marshal(v)
	// }
	// if err != nil {
	// 	return err
	// }
	// return txn.Set(key, dta)
}

// callOnChange loops through OnChange listeners and calls them.
func (st *Store[T]) callOnChange(id string, before, after store.Nullable[T]) {
	for _, cb := range st.onChange {
		cb(id, before, after, nil)
	}
}

// callBeforeChange loops through BeforeChange listeners and calls them.
func (st *Store[T]) callBeforeChange(id string, before, after store.Nullable[T]) error {
	for _, cb := range st.beforeChange {
		err := cb(id, before, after, nil)
		if err != nil {
			return err
		}
	}
	return nil
}
