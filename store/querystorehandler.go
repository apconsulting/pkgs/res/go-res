package store

import (
	"fmt"
	"net/url"

	res "gitlab.com/apconsulting/pkgs/res/go-res"
)

// QueryHandler is a res.Service handler for get requests that fetches the
// results from an underlying QueryStore. It also listens to changes in the
// QueryStore, and sends events if the change affects the query.
type QueryHandler[T any] struct {
	// A QueryStore from where to fetch the resource references.
	QueryStore QueryStore[T]
	// QueryRequestHandler transforms an external query request and path param
	// values into a query that can be handled by the QueryStore. The
	// QueryHandler will respond with a query resource.
	//
	// A non-empty normalized query string based on the url.Values must be
	// returned. See:
	// https://resgate.io/docs/specification/res-service-protocol/#get-request
	//
	// Must not be set if RequestHandler is set.
	QueryRequestHandler func(rname string, pathParams map[string]string, q url.Values) (url.Values, string, error)
	// RequestHandler transforms an external request and path param values into
	// a query that can be handled by the QueryStore.
	//
	// Must not be set if QueryRequestHandler is set.
	RequestHandler func(rname string, pathParams map[string]string) (url.Values, error)
	// Transformer transforms the internal query results and events into an
	// external resource, and events for the external resource.
	Transformer QueryTransformer[T]
	// AffectedResources is called on query change, and should return a list of
	// resources affected by the change.
	//
	// This is only required if the resource contains path parameters.
	//
	// Example
	//
	// If a the handler listens for requests on: library.books.$firstLetter A
	// change of a book's name from "Alpha" to "Beta" should have
	// AffectedResources return the following:
	//
	//  []string{"library.books.a", "library.books.b"}
	AffectedResources func(res.Pattern, QueryChange[T]) []string
}

var _ res.Option = QueryHandler[any]{}

type queryHandler[T any] struct {
	s       *res.Service
	pattern res.Pattern
	typ     res.ResourceType
	qs      QueryStore[T]
	qrh     func(string, map[string]string, url.Values) (url.Values, string, error)
	rh      func(string, map[string]string) (url.Values, error)
	trans   QueryTransformer[T]
	ar      func(res.Pattern, QueryChange[T]) []string
	isQuery bool
}

// WithQueryStore returns a new QueryHandler value with QueryStore set to
// qstore.
func (qh QueryHandler[T]) WithQueryStore(qstore QueryStore[T]) QueryHandler[T] {
	qh.QueryStore = qstore
	return qh
}

// WithQueryRequestHandler returns a new QueryHandler value with
// QueryRequestHandler set to f.
func (qh QueryHandler[T]) WithQueryRequestHandler(f func(rname string, pathParams map[string]string, q url.Values) (url.Values, string, error)) QueryHandler[T] {
	qh.QueryRequestHandler = f
	return qh
}

// WithRequestHandler returns a new QueryHandler value with RequestHandler set
// to f.
func (qh QueryHandler[T]) WithRequestHandler(f func(rname string, pathParams map[string]string) (url.Values, error)) QueryHandler[T] {
	qh.RequestHandler = f
	return qh
}

// WithTransformer returns a new QueryHandler value with Tranformer set to
// transformer.
func (qh QueryHandler[T]) WithTransformer(transformer QueryTransformer[T]) QueryHandler[T] {
	qh.Transformer = transformer
	return qh
}

// WithAffectedResources returns a new QueryHandler value with IDToRID set to f.
func (qh QueryHandler[T]) WithAffectedResources(f func(res.Pattern, QueryChange[T]) []string) QueryHandler[T] {
	qh.AffectedResources = f
	return qh
}

// SetOption is to implement the res.Option interface.
func (qh QueryHandler[T]) SetOption(h *res.Handler) {
	if qh.QueryStore == nil {
		panic("no QueryStore set")
	}
	if qh.QueryRequestHandler != nil && qh.RequestHandler != nil {
		panic("both RequestHandler and QueryRequestHandler are set")
	}
	o := queryHandler[T]{
		qs:      qh.QueryStore,
		qrh:     qh.QueryRequestHandler,
		rh:      qh.RequestHandler,
		trans:   qh.Transformer,
		ar:      qh.AffectedResources,
		isQuery: qh.QueryRequestHandler != nil,
	}
	h.Option(res.OnRegister(o.onRegister))
	// Set conditional handler methods depending if we have
	// an ordinary resource or a query resource.
	if o.isQuery {
		h.Option(res.GetResource(o.getQueryResource))
		o.qs.OnQueryChange(o.queryChangeHandler)
	} else {
		h.Option(res.GetResource(o.getResource))
		o.qs.OnQueryChange(o.changeHandler)
	}
}

func (o *queryHandler[T]) onRegister(s *res.Service, p res.Pattern, h res.Handler) {
	if res.Pattern(p).IndexWildcard() >= 0 {
		if o.ar == nil {
			panic("QueryHandler requires an AffectedResources callback when handling resources with tags or wildcards: " + string(p))
		}
	}
	if h.Type == res.TypeUnset {
		panic("no Type is set")
	}
	if h.Type != res.TypeModel && h.Type != res.TypeCollection {
		panic("Type must be set to TypeModel or TypeCollection")
	}
	o.s = s
	o.pattern = p
	o.typ = h.Type
}

func (o *queryHandler[T]) getResource(r res.GetRequest) {
	var err error
	var q url.Values
	if o.rh != nil {
		q, err = o.rh(r.ResourceName(), r.PathParams())
		if err != nil {
			r.Error(err)
			return
		}
	}

	result, err := o.getResult(q)
	if err != nil {
		r.Error(err)
		return
	}

	switch o.typ {
	case res.TypeModel:
		r.Model(result)
	case res.TypeCollection:
		r.Collection(result)
	default:
		panic("invalid type")
	}
}

func (o *queryHandler[T]) getQueryResource(r res.GetRequest) {
	q, norm, err := o.qrh(r.ResourceName(), r.PathParams(), r.ParseQuery())
	if err != nil {
		r.Error(err)
		return
	}

	result, err := o.getResult(q)
	if err != nil {
		r.Error(err)
		return
	}

	if norm == "" {
		panic("QueryResourceHandler returned an empty normalized query string for resource: " + r.ResourceName())
	}

	switch o.typ {
	case res.TypeModel:
		r.QueryModel(result, norm)
	case res.TypeCollection:
		r.QueryCollection(result, norm)
	default:
		panic("invalid type")
	}

}

func (o *queryHandler[T]) queryChangeHandler(qc QueryChange[T]) {
	if o.ar != nil {
		qrids := o.ar(o.pattern, qc)
		for _, qrid := range qrids {
			o.queryEvent(qrid, qc)
		}
	} else {
		o.queryEvent(string(o.pattern), qc)
	}
}

func (o *queryHandler[T]) changeHandler(qc QueryChange[T]) {
	if o.ar != nil {
		rids := o.ar(o.pattern, qc)
		for _, rid := range rids {
			if err := o.resourceEvent(rid, qc); err != nil {
				o.errorf("QueryHandler encountered error generating events for resource %s: %s", rid, err)
				return
			}
		}
	} else {
		if err := o.resourceEvent(string(o.pattern), qc); err != nil {
			o.errorf("QueryHandler encountered error generating events for resource %s: %s", o.pattern, err)
		}
	}
}

func (o *queryHandler[T]) resourceEvent(rid string, qc QueryChange[T]) error {
	r, err := o.s.Resource(rid)
	if err != nil {
		return fmt.Errorf("error getting resource: %s", err)
	}

	var q url.Values
	if o.rh != nil {
		q, err = o.rh(r.ResourceName(), r.PathParams())
		if err != nil {
			return fmt.Errorf("error calling RequestHandler: %s", err)
		}
	}

	// Get events. We assume Events will will by itself determine if
	// the query is affected or not, by returning a no events.
	evs, reset, err := qc.Events(q)
	if err != nil {
		return fmt.Errorf("error getting events: %s", err)
	}

	if reset {
		r.ResetEvent()
		return nil
	}

	if len(evs) == 0 {
		return nil
	}

	for _, ev := range evs {
		switch ev.Name {
		case "add":
			if o.trans != nil {
				r.AddEvent(o.trans.TransformEvent(ev).Value, ev.Idx, nil)
			} else {
				r.AddEvent(ev.Value, ev.Idx, nil)
			}
		case "remove":
			r.RemoveEvent(ev.Idx, nil)
		case "change":
			r.ChangeEvent(ev.Changed, nil)
		default:
			return fmt.Errorf("invalid event name: %s", ev.Name)
		}
	}
	return nil
}

func (o *queryHandler[T]) queryEvent(qrid string, qc QueryChange[T]) {
	qcr, err := o.s.Resource(qrid)
	if err != nil {
		panic(err)
	}
	qcr.QueryEvent(func(qreq res.QueryRequest) {
		// Nil means end of query event.
		if qreq == nil {
			return
		}

		q, _, err := o.qrh(qreq.ResourceName(), qreq.PathParams(), qreq.ParseQuery())
		if err != nil {
			qreq.Error(err)
			return
		}

		// Get events for the query
		evs, reset, err := qc.Events(q)
		if err != nil {
			qreq.Error(err)
			return
		}

		// Handle reset
		if reset {
			result, err := o.getResult(q)
			if err != nil {
				qreq.Error(err)
				return
			}
			switch o.typ {
			case res.TypeModel:
				qreq.Model(result)
			case res.TypeCollection:
				qreq.Collection(result)
			default:
				panic("invalid type")
			}
			return
		}

		if len(evs) == 0 {
			return
		}

		for _, ev := range evs {
			switch ev.Name {
			case "add":
				if o.trans != nil {
					qreq.AddEvent(o.trans.TransformEvent(ev), ev.Idx, nil)
				} else {
					qreq.AddEvent(ev.Value, ev.Idx, nil)
				}
			case "remove":
				qreq.RemoveEvent(ev.Idx, nil)
			case "change":
				qreq.ChangeEvent(ev.Changed, nil)
			default:
				panic("invalid event name: " + ev.Name)
			}
		}
	})
}

func (o *queryHandler[T]) getResult(q url.Values) (interface{}, error) {
	result, err := o.qs.Query(q)
	if err != nil {
		return nil, err
	}

	// Transform results
	if o.trans != nil {
		refs, err := o.trans.TransformResult(result)
		if err != nil {
			return nil, err
		}
		return refs, nil
	}
	return result, nil
}

func (o *queryHandler[T]) errorf(format string, v ...interface{}) {
	l := o.s.Logger()
	if l != nil {
		l.Errorf(format, v...)
	}
}
